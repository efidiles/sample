import React, { Component } from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
// import { get } from 'lodash'
import createDebugger from 'debug'
import './App.css'
import ClassComponent from './templates/ClassComponent'
import ClassComponentWithHooks from './templates/ClassComponentWithHooks'
import ReduxContainer from './templates/ReduxContainer'
import StatelessComponent from './templates/StatelessComponent'

const debug = createDebugger('App')

function rootReducer(state = {}, action) {
  return state
}

const Store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

class App extends Component {
  render() {
    debug('test')
    return (
      <Provider store={Store}>
        <div className="App">
          <ClassComponent/>
          <ClassComponentWithHooks/>
          <ReduxContainer/>
          <StatelessComponent/>
        </div>
      </Provider>
    )
  }
}

export default App
