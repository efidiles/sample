import React, { Component } from 'react'
import { connect } from 'react-redux'

class ReduxContainer extends Component {
  render() {
    return (
      <div>
        ReduxContainer
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReduxContainer)
