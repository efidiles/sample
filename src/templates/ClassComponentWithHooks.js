import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ClassComponentWithHooks extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  shouldComponentUpdate(nextProps, nextState) {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    return (
      <div>
        ClassComponentWithHooks
      </div>
    )
  }
}

export default ClassComponentWithHooks
