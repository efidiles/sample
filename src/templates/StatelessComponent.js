import React from 'react'

const StatelessComponent = () => {
  return (
    <div>
      StatelessComponent
    </div>
  )
}

export default StatelessComponent
