import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ClassComponent extends Component {
  render() {
    return (
      <div>
        ClassComponent
      </div>
    )
  }
}

export default ClassComponent
